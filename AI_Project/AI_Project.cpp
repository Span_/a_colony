﻿// AI_Project.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "pch.h"
#include <iostream>
#include <math.h>
#include <string>

//class generic_func {
//private:
//	float learnRate = 1.0e-10f;
//	//types of possible basis functions
//	enum basis {
//		PERIODIC = 0,
//		POLYNOMIAL = 1
//	};
//	int num_params;
//	//parameters for basis function
//	float * params; 
//	//float * inputs;
//
//	float compute_polynomial(float* inputs) {
//		//this is a polynomial, like a taylor series expansion
//		//p0 + p1 * x0 ^ 1 + ... pn * xn-1 ^ n
//
//		float output = params[0];
//		for (int i = 1; i < sizeof(params); i++) {
//			output += pow(params[i] * inputs[i - 1], (float)i);
//		}
//		return output;
//	}
//
//public:
//	//initialzize 
//	generic_func(float* inputs) {
//		//one set of params per output, one general function per output
//		num_params = sizeof(inputs) + 1;
//		params = new float[num_params];
//		
//		//init values of params as 0;
//		for (int i = 0; i < sizeof(params); i++) {
//			params[i] = 0;
//		}
//		
//	}
//	//run a computation
//	float run(float* inputs) {
//		float output = compute_polynomial(inputs);
//		return output;
//		
//	}
//
//	//process feedback
//	void update(float* inputs, float* outputs, float* correct_outputs) {
//		int p = 0;
//		//gradient discent
//		for (int a = 0; a < sizeof(outputs); a++) {
//			params[p] = params[p] - learnRate * 1 * (outputs[a] - correct_outputs[a]);
//			p++;
//			for (int n = 1; n < num_params + 1; n++) {
//
//
//				for (int i = 0; i < sizeof(inputs); i++) {
//
//					params[p] = params[p] - learnRate * pow(inputs[i], n) * (outputs[a] - correct_outputs[a]);
//					p++;
//
//				}
//
//			}
//		}
//	}
//
//	void print_params() {
//		for (int i = 0; i < sizeof(params); i++) {
//			std::cout << std::to_string(params[i]) << ", ";
//		}
//		std::cout << "\n";
//	}
//};

//class colony_member {
//private:
//public:
//	colony_member() {
//
//	}
//};

enum member_type
{
	GIVER,
	TAKER,
	INPUT,
	OUTPUT,
	NONE
};

class colony {
	//stored within
	//taking from
private:
	int num_members = 1000; //user sets
	int max_connections = 10; //user sets
	int num_inputs; //user determines
	int num_outputs; //user determines
	int num_connections;

	double init_member_val = 0.0;
	int init_connection_val = -1;

	double m_val_decay_rate = 0.9;
	double m_trigger_val = 1.0;
	//double taker_weight = 1.0;


	double give_take_split = 70;//percent of members who are givers
	int init_health = 0;
	int health_incr = 1;

	//array of members
	double * members;
	//list of connections for each member
	int * connections_incoming;
	//list of outgoing connections for each member
	int * connections_outgoing;

	//track time since last useage for incomming connection
	int * inc_connection_health;

	//member_type for each member
	member_type * m_type;

public:
	colony() {
		//init
	}

	/*~colony() {
		delete[] members;
		delete[] connections_incoming;
		delete[] connections_outgoing;
		delete[] inc_connection_health;
		delete[] m_type;
	}*/

	//get/set for num_members
	bool set_num_members(int num) {

		if (num_members != -1 || num <= 0) {
			//improper value fail state
			return false;
		}
		if (num_members == -1) {
			num_members = num;
			return true;
		}
	}

	int get_num_members() {
		return num_members;
	}

	//get/set for max_connections
	bool set_max_connections(int max) {
		if (max_connections != -1 || max <= 0) {
			//improper value fail state
			return false;
		}
		if (max_connections == -1) {
			max_connections = max;
			return true;
		}
		max_connections = max;
	}

	int get_max_connections() {
		return max_connections;
	}

	bool init_members() {

		//check values
		if (num_members <= 0) {
			std::cout << "no members\n";
			return false;
		}

		if (num_inputs + num_outputs > num_members) {
			//not enough members
			std::cout << "too many inputs and outputs for member count\n";
			return false;
		}

		//make members list
		members = new double[num_members];

		//init values
		for (int i = 0; i < num_members; i++) {
			members[i] = init_member_val;
		}

		//setting member types here:
		m_type = new member_type[num_members];

		int m = 0;
		for (int i = 0; i < num_inputs; i++) {
			//set inputs
			m_type[m] = INPUT;
			m++;
		}
		for (int i = 0; i < num_outputs; i++) {
			//set outputs
			m_type[m] = OUTPUT;
			m++;
		}
		for (int i = m; i < num_members; i++) {

			//pick random giver / taker neurons
			if (rand() % 100 < give_take_split) {
				m_type[i] = GIVER;
			}
			else {
				m_type[i] = TAKER;
			}
		}
	}

	bool init_connections() {

		if (num_members <= 0 || max_connections <= 0) {
			//invalid init values
			return false;

		}
		num_connections = num_members * max_connections;

		connections_incoming = new int[num_connections];

		for (int i = 0; i < num_connections; i++) {
			//clear all the connections to the default value
			connections_incoming[i] = init_connection_val;
		}

		connections_outgoing = new int[num_connections];

		for (int i = 0; i < num_connections; i++) {
			//clear all the connections to the default value
			connections_outgoing[i] = init_connection_val;
		}

		inc_connection_health = new int[num_connections];

		for (int i = 0; i < num_connections; i++) {
			//set init value
			inc_connection_health[i] = init_health;
		}

		//make random connections
		//int rand_c_num;
		//int rand_m_index;
		//bool set_c = false; ///did we successfully find an open spot to connect to?
		//int free_con;
		//go through the list of members
		for (int i = 0; i < num_members; i++) {


			switch (m_type[i])
			{
			case INPUT:
				break;
			case OUTPUT:
				init_rnd_connections(i);
				break;
			case GIVER:
				init_rnd_connections(i);
				break;
			case TAKER:
				init_rnd_connections(i);
				break;
			default:
				break;
			}

		}

		return true;
	}

	bool step(double * input, double * output) {


		int input_index = 0;
		int output_index = 0;

		for (int i = 0; i < num_members; i++) {
			//go through all the members, assign roles
			switch (m_type[i])
			{
			case INPUT:
				//gain the value of the input
				members[i] += input[input_index];
				
				if (members[i] > m_trigger_val) {
				
					deliver_payload(i, 1.0);

				}

				input_index++;
				break;
			case OUTPUT:
				//apply the output
				if (members[i] > init_member_val) {
					output[output_index] = members[i];
					members[i] = init_member_val;
				}
				//if any connections have bad health, try to reset them
				check_connection_health(i);

				output_index++;
				break;
			case GIVER:
				//trigger if reached limit
				//reset incomming connection if health too low

				if (members[i] > m_trigger_val) {
					//could there be fat bois?

					deliver_payload(i, 1.0);

				}
				else {
					//decay back towards 0
					members[i] *= m_val_decay_rate;
				}
				//check incomming connection health
				//if any connections have bad health, try to reset them
				check_connection_health(i);
				break;
			case TAKER:
				if (members[i] > m_trigger_val) {
					//could there be fat bois?

					deliver_payload(i, -1.0);
				}
				else {
					//decay back towards 0
					members[i] *= m_val_decay_rate;
				}
				//check incomming connection health
				//if any connections have bad health, try to reset them
				check_connection_health(i);
				break;
			default:
				break;
			}
		}
		return true;
	}

	void print_arrays() {

		std::cout << "Colony\n";
		for (int i = 0; i < num_members; i++) {
			std::cout << std::to_string(members[i]) << "\n";
		}
		std::cout << "\nConnections\n";
		int nl = 0;
		for (int i = 0; i < num_connections; i++) {
			std::cout << std::to_string(connections_incoming[i]) << ", ";
			nl++;
			if (nl >= max_connections) {
				nl = 0;
				std::cout << "\n";
			}
		}
		nl = 0;
		std::cout << "\nConnection Health\n";
		for (int i = 0; i < num_connections; i++) {
			std::cout << std::to_string(inc_connection_health[i]) << ", ";
			nl++;
			if (nl >= max_connections) {
				nl = 0;
				std::cout << "\n";
			}
		}

	}

	void modify_connection_health(int amount) {
		//modify connection health of all members by amount
		//This is to handle reward / penalty of global actions. 
		for (int i = 0; i < num_connections; i++) {
			inc_connection_health[i] += amount;
		}

	}

///////////////////////////////////////////////////////////////////////////////////////////////////
private:
///////////////////////////////////////////////////////////////////////////////////////////////////


	bool deliver_payload(int i, double give_take) {
		//member i, giver (1.0) or taker (-1.0)
		//pass off to connections
		double c_tot = 0.0;
		
		for (int c = 0; c < max_connections; c++) {
			//get number of connections
			if (connections_outgoing[c + i * max_connections] >= 0) c_tot += 1.0;
		}

		if (c_tot < 0.9) {
			//need to create a new connection
			return false;
		}
		
		//amount to apply to outgoing connections
		double give_take_amount = give_take * m_trigger_val / c_tot;

		for (int c = 0; c < max_connections; c++) {
			//distribute load equally
			if (connections_outgoing[c + i * max_connections] >= 0) {
				//each connection is the index of the member it is connected to
				members[connections_outgoing[c + i * max_connections]] += give_take_amount;
				inc_connection_health[connections_outgoing[c + i * max_connections]] += health_incr;
			}
		}

		members[i] -= m_trigger_val;
		return true;
	}

	void init_rnd_connections(int i) {

		//choose random number of connections to make
		int rand_c_num = rand() % (max_connections - 1) + 1; //exclude 0
		int free_con;
		int rand_m_index;
		//go through each connection and try to set it to another
		for (int n = 0; n < rand_c_num; n++) {
			//pick a random member to connect to 
			rand_m_index = rand() % num_members;

			free_con = get_outgoing_connection_index(rand_m_index, init_connection_val);

			try_connection(i, n, rand_m_index);
			/*if (free_con != -1 && rand_m_index != i) {
				//don't connect to yourself
				//make the connection
				//set my incoming to them
				connections_incoming[n + i * max_connections] = rand_m_index;
				//set their outgoing to me
				connections_outgoing[free_con + rand_m_index * max_connections] = i;

			}*/
			//TODO: else{ keep trying to find another opening?}
		}
	}

	int get_outgoing_connection_index(int a, int b) {
		//returns the index of the 'b' connection spot in the outgoing connections of member 'a'.
		//returns -1 if none is found
		//look for member 'b' or empty connection value
		for (int i = 0; i < max_connections; i++) {
			if (connections_outgoing[i + a * max_connections] == b) return i;
		}
		return -1;
	}

	int get_incoming_connection_index(int a, int b) {
		//returns the index of the 'b' connection spot in the incoming connections of member 'a'.
		//returns -1 if none is found
		//look for member 'b' or empty connection value
		for (int i = 0; i < max_connections; i++) {
			if (connections_incoming[i + a * max_connections] == b) return i;
		}
		return -1;
	}

	bool try_connection(int a, int a_con, int b) {
		//take member 'a' connection a_con and try to put it on 'b's outgoing connection list
		//input member index a

		int con_index = get_outgoing_connection_index(b, a);
		if (con_index != init_connection_val && b != a && m_type[b] != OUTPUT) {
			//don't connect to yourself
			//don't take from an output
			//make the connection
			//set my incoming to them
			connections_incoming[a_con + a * max_connections] = b;
			//set their outgoing to me
			connections_outgoing[con_index + b * max_connections] = a;
			return true;
		}
		return false;
	}

	void check_connection_health(int a) {
		//check incomming connection health for member 'a'
		//pick a random member 'c' to try connection if bad connection health with member 'b'
		int b; //other member connected to
		int b_con; //b's connection index for a
		int c;
		int c_con;
		for (int a_con = 0; a_con < max_connections; a_con++) {
			if (inc_connection_health[a_con + a * max_connections] <= 0) {

				//pick random to try to connect to
				c = rand() % num_members;
				c_con = get_outgoing_connection_index(c, init_connection_val);
				if (c != a && c_con != -1) {
					//reset connection
					b = connections_incoming[a_con + a * max_connections];
					connections_incoming[a_con + a * max_connections] = -1;
					b_con = get_outgoing_connection_index(a, b);
					connections_outgoing[b_con + b * max_connections] = -1;

					//set connections
					connections_incoming[a_con + a * max_connections] = c;
					connections_outgoing[c_con + c * max_connections] = a;
				}
			}
		}
		
	}

	void delete_member(int a) {
		//delete member a from the list of members
		//clear member type
		//clear the incoming connections list for this member 'a'
		//clear the outgoing connections for any member 'b' that connects to 'a'
		//clear connection health for member 'a'
		//clear outgoing connections for member 'a'
		//clear the incoming connections for any member 'b' that connects to 'a'
		//clear connection health for any member 'b' that connects to 'a'


		members[a] = init_member_val;
		m_type[a] = NONE;

		int c_index;
		int b_index;
		//delete inc connections & associated outgoing connections
		for (int i = 0; i < num_connections; i++) {
			b_index = connections_incoming[i + a * max_connections];
			c_index = get_outgoing_connection_index(b_index, a);

			if (c_index != init_connection_val) {
				//clear the outgoing connections for any member 'b' that connects to 'a'
				connections_outgoing[c_index + b_index * max_connections] = init_connection_val;
			}
			//clear the incoming connections list for this member 'a'
			connections_incoming[i + a * max_connections] = init_connection_val;

			//clear connection health for member 'a'
			inc_connection_health[i + a * max_connections] = init_connection_val;

			//clear the incoming connections for any member 'b' that connects to 'a'
			b_index = connections_outgoing[i + a * max_connections];
			c_index = get_incoming_connection_index(b_index, a);
			if (c_index != init_connection_val) {
				connections_incoming[c_index + b_index * max_connections] = init_connection_val;

				//clear connection health for any member 'b' that connects to 'a'
				inc_connection_health[c_index + b_index * max_connections] = init_connection_val;
			}

			//clear outgoing connections for member 'a'
			connections_outgoing[i + a * max_connections] = init_connection_val;

		}
	}
	
};

int main()
{
    std::cout << "Hello World!\n";
	/*float inputs[] = { 0.0f, 0.0f, 0.0f };
	float outputs[3];
	float correct_outputs[] = { 1.0f, 1.0f, 1.0f };

	generic_func test_func = generic_func(inputs);
	test_func.run(inputs);
	test_func.update(inputs, outputs, correct_outputs);
	test_func.print_params();*/

	const int num_inputs = 8;
	const int num_outputs = 8;
	double inputs[num_inputs];
	double outputs[num_outputs];
	for (int i = 0; i < num_inputs; i++) {
		inputs[i] = 1.0;
	}
	for (int i = 0; i < num_outputs; i++) {
		outputs[i] = 0.0;
	}

	colony c;
	c.init_members();
	c.init_connections();
	int iterations = 1000;
	int output_sum = 0;
	/*for (int i = 0; i < iterations; i++) {
		c.step(inputs, outputs);

		for (int j = 0; j < num_outputs; j++) {
			output_sum += outputs[j];
			//add up all the outputs, if we are > 0, add to connection health, otherwise subtract from chealth
		}
		if (output_sum > 0.0) {
			c.modify_connection_health(1);
		}
		else {
			c.modify_connection_health(-1);
		}
		output_sum = 0;
	}*/
	c.print_arrays();

	system("pause");

}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
